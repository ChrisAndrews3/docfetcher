/*
	Author: Chris Andrews
	Created Date: 10/30/2015
	Modified Date: 10/31/2015
*/
#using <mscorlib.dll>
#using <System.dll>

#include <time.h>
#include <tchar.h>

using namespace System;
using namespace System::Net;
using namespace System::IO;
using namespace System::Text;

CookieCollection^ fakeCookies(){
	// Forge the cookies from what I have in chrome
	CookieCollection^ theContainer = gcnew CookieCollection;
	theContainer->Add(gcnew Cookie("EQMApp", "FC719012499A3FEB1511C2FE6C62FA288D026471F8FF7BA1DA36306BBBFCAF843E8B339A61ABBBDA9A28AC66DA114D5EBC19CDC7623C126A5594F9665A3F0812B8B5A813DA425822D8EE02F3F5879D85AE28C5D153326C62B2E4E24847BC61F160892FE5", "/", "asr.remoteauditor.com"));
	theContainer->Add(gcnew Cookie("ASP.NET_SessionId", "5yhnnhno0xq53icylozntvlq", "/", "asr.remoteauditor.com"));
	theContainer->Add(gcnew Cookie("EQMDataCache5yhnnhno0xq53icylozntvlq", "", "/", "asr.remoteauditor.com"));
	return theContainer;
}

 int main(){
	clock_t t1, t2;
	String ^sURL, ^sName, ^readPath, ^writePath, ^suffix;
	int counter = 1;

	// Quick conversation with the user, then start the clock
	// TODO: If the directory doesn't exist ask the user if they want to create it
	// TODO: I'm tired of typing the same shit over and over again
	Console::WriteLine("Where should I read from? (File)");
	readPath = Console::ReadLine();
	Console::WriteLine("Where should I write to? (EXISTING Directory \"\\\")");
	writePath = Console::ReadLine();
	t1 = clock();

	if (!writePath->EndsWith("\\")){
		writePath += "\\";
	}

	// Open the document that has our list of URLs
	FileStream^ urls = gcnew FileStream(readPath, IO::FileMode::Open);

	// Pipe the stream to a higher level stream reader with the required encoding format. 
	StreamReader^ readStream = gcnew StreamReader( urls,Encoding::UTF8 );

	while(sURL = readStream->ReadLine()){
		// Build a request for url and add our forged cookies
		HttpWebRequest^ request = dynamic_cast<HttpWebRequest^>(WebRequest::Create(sURL));
		request->CookieContainer = gcnew CookieContainer;
		request->CookieContainer->Add(fakeCookies());

		// Modify the url string turn it into document name.
		// This is kludgey as hell, TODO: Fix this garbage
		if (sURL->LastIndexOf("Type.ItemAuditQualityManual/") > -1){
			sName = sURL->Substring(138);
		}else if (sURL->LastIndexOf("Type.ItemAuditTargetQualityManual/") > -1){
			sName = sURL->Substring(154);
		}else if (sURL->LastIndexOf("Type.ClientItem") > -1){
			sName = sURL->Substring(125);
		}else if (sURL->LastIndexOf("Display.aspx/") > -1){
			sName = sURL->Substring(63);
		}
		if (sName->LastIndexOf('.') > -1){
			suffix = sName->Substring(sName->LastIndexOf('.'));
		}else{
			suffix = "";
		}
		sName = sName->Substring(0, 36) + suffix;

		// Perform the HTTP request, get a response and tell the user what's happening
		Console::WriteLine( "Fetching: {0}\n", sName);

		try{
			// Get a response and tell the user about it
			HttpWebResponse^ response = dynamic_cast<HttpWebResponse^>(request->GetResponse());
			if (response->ResponseUri->ToString()->LastIndexOf("ErrorPage") > -1){
				Console::WriteLine("EQM redirected to error page, we're skipping #{0}", counter);
			}else{
				Console::WriteLine( "Content length is {0}", response->ContentLength );
				Console::WriteLine( "Content type is {0}\n", response->ContentType );

				// Get the stream associated with the response.
				Stream^ receiveStream = response->GetResponseStream();

				// Open a stream to a file and copy from one stream to another
				Console::Write("Saving #{0}... ", counter);
				FileStream^ writeStream = gcnew FileStream(writePath + sName, IO::FileMode::Create);
				receiveStream->CopyTo(writeStream);
	
				// Success! let's share the good news, increment the counter and close them streams
				Console::WriteLine("Done\n");
				
				receiveStream->Close();
				writeStream->Close();
			}
			counter++;
			response->Close();

		}
		catch( WebException^ we ){
			HttpWebResponse^ errorResponse = dynamic_cast<HttpWebResponse^>(we->Response);
			if (errorResponse->StatusCode == HttpStatusCode::NotFound) {
				Console::WriteLine( "404, we're skipping that one" );
			}else{
				throw we;
			}
		}		
	}

	// Get the end clock and calculate the time it took
	t2 = clock();
	float diff = ((float)t2-(float)t1);
	float seconds = diff / CLOCKS_PER_SEC;
	TimeSpan^ t = TimeSpan::FromSeconds(seconds);

	// We did it! Again, share the good news and close them streams
	Console::WriteLine("All Done! It only took {0}:{1}:{2}", t->Hours, t->Minutes, t->Seconds);
	readStream->Close();

	Console::ReadLine();

	return 0;
}